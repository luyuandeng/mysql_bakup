#!/bin/bash

chmod +x /backup.sh

if [[ ${DB_DUMP_CRON} == "" ]]; then
	echo "Missing DB_DUMP_CRON env variable"
	exit 1
fi
echo  "==== ${DB_DUMP_CRON}========"
 
DB_DUMP_CRON="*/5  * * * *"

CRON_STRINGS="$DB_DUMP_CRON /backup.sh >> /var/log/cron/cron.log 2>&1"

echo -e "$CRON_STRINGS\n" >> /var/spool/cron/crontabs/root

crontab -l

chmod -R 0644 /var/spool/cron/crontabs

# run once start
if [ -n "$DUMP_ONCE_START" -a "$DUMP_ONCE_START" = "true" ]; then
    /backup.sh >> /var/log/cron/cron.log 2>&1 &
fi

# crond running in background and log file reading every second by tail to STDOUT
#crond -s /var/spool/cron/crontabs -b -L /var/log/cron/cron.log "$@" && tail -f /var/log/cron/cron.log
crond -S /var/spool/cron/crontabs -b -L /var/log/cron/cron.log "$@" && tail -f /var/log/cron/cron.log
